Proof of concept for an 02 website crawler that retrieves the monthly landline
calls pricing for a list of countries.

Code is just a simple module, written in Python 3. It can be executed directly
from the shell with the command ``python crawl.py``.

It will accept some optional arguments to set up the URL and the Selenium
browser to be used. When executed without any arguments, the command just
defaults to the expected values. To check the arguments and its usage, just
execute the script with the 'help' parameter: ``python crawl.py --help``.

There is also a couple of unit tests that can be executed by issuing the
command ``python test.py``. However, that could be extended to some more tests
to check the scrape operations a bit more thorough.

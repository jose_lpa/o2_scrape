import unittest
from unittest.mock import MagicMock, patch

from selenium.common.exceptions import NoSuchElementException

from crawl import lookup_monthly


class CrawlTestCase(unittest.TestCase):
    def test_bad_url_no_string(self):
        """
        An ``AssertionError`` exception is raised when the ``url`` parameter is
        not a string.
        """
        self.assertRaisesRegex(
            AssertionError,
            'Please, provide a URL.',
            lookup_monthly,
            12345
        )

    def test_bad_browser_no_string(self):
        """
        An ``AssertionError`` exception is raised when the ``browser`` parameter
        is not a string.
        """
        self.assertRaisesRegex(
            AssertionError,
            'Please, provide a browser ID.',
            lookup_monthly,
            'http://www.google.es', 12345
        )

    @patch('selenium.webdriver.Firefox')
    def test_selenium_browser_not_available(self, mock):
        """
        A ``RuntimeError`` exception is raised when the selected Selenium
        Webdriver browser is not available in the system.

        This test doesn't effectively call the Selenium browser. Instead, it
        just uses the Python 3 ``mock`` standard library to simulate that the
        ``selenium.webdriver.Firefox`` object was instantiated and it returned
        the Selenium default exception raised when it is not available.

        To run this unit test in Python 2, the ``mock`` library must be installed
        apart and then it doesn't come from ``unittest`` module, but from its
        own.
        """
        # Let's provoke an error when calling `Firefox()` :)
        mock.side_effect = FileNotFoundError

        self.assertRaisesRegex(
            RuntimeError,
            'The provided browser Firefox is not available. Please, provide a '
            'valid browser.',
            lookup_monthly
        )

    @patch('selenium.webdriver.Firefox')
    def test_selenium_browser_element_not_found(self, mock):
        find = mock().find_element_by_id = MagicMock()
        find.side_effect = NoSuchElementException

        self.assertRaisesRegex(
            RuntimeError,
            'Unable to retrieve any pricing. Form was not detected.',
            lookup_monthly
        )

        self.assertTrue(mock.close.called)


if __name__ == '__main__':
    unittest.main()

import argparse

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys


COUNTRIES = [
    'Canada', 'Germany', 'Iceland', 'Pakistan', 'Singapore', 'South Africa'
]


def lookup_monthly(url=None, browser='Firefox', countries=COUNTRIES):
    if url is None:
        url = 'http://international.o2.co.uk/internationaltariffs/calling_abroad_from_uk'
    else:
        assert isinstance(url, str), 'Please, provide a URL.'
        assert isinstance(browser, str), 'Please, provide a browser ID.'

    try:
        driver = getattr(webdriver, browser)()
    except (AttributeError, FileNotFoundError):
        raise RuntimeError(
            'The provided browser {} is not available. Please, provide a valid'
            ' browser.'.format(browser)
        )

    # Looks like everything is ok. Start page scrape.
    driver.get(url)

    # Obtain the submission form.
    try:
        input_field = driver.find_element_by_id('countryName')
    except NoSuchElementException:
        driver.close()
        raise RuntimeError(
            'Unable to retrieve any pricing. Form was not detected.')

    prices = {}

    for country in countries:
        # Clean the form field.
        input_field.clear()

        # Submit the current country.
        ActionChains(driver).move_to_element(input_field).send_keys(
            country, Keys.ENTER).perform()

        # Try to find the monthly pricing button.
        try:
            button = driver.find_element_by_id('paymonthly')
            button.click()
            price = driver.find_element_by_xpath(
                '//div[@class="borderBox1 clearfix intrCallCardbg"]'
                '/table[@id="rateTable"]/tbody/tr/td[@id="landLine"]/strong'
            )
            prices[country] = price.text
        except NoSuchElementException:
            print("Unable to retrieve pricing for '{}'.".format(country))

    # Close browser after finish.
    driver.close()

    return prices


if __name__ == '__main__':
    # Make URL and Selenium Webdriver browser selectable by the user. Defaults
    # to the expected values if user doesn't provide anything.
    parser = argparse.ArgumentParser(
        description='Crawls O2 website looking for monthly pricings.')
    parser.add_argument(
        '--url',
        dest='url',
        default=None,
        help='Select a different URL'
    )
    parser.add_argument(
        '--browser',
        dest='browser',
        default='Firefox',
        help='Select a different browser'
    )

    args = parser.parse_args()

    try:
        prices = lookup_monthly(args.url, args.browser)

        # Print prices list.
        for key, value in prices.items():
            print('{0}: {1} per minute'.format(key, value))
    except (RuntimeError, AssertionError) as exc:
        print('ERROR: ', exc)
